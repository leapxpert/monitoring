# Monitoring

## Scraping App Metrics

Files in the `app` directory of this repo include two versions of a Prometheus scraper ([metrics1.py](app/metrics1.py), [metrics2.py](app/metrics2.py)), and [app-deployment-sample.yaml](app/app-deployment-sample.yaml) - a sample Kubernetes deployment file to deploy the app being monitored and the metrics scraper as separate containers within the same Kubernetes pod.

Notes on `metricsX.py` scripts:
* [metrics1.py](app/metrics1.py) implements scraping the way Prometheus does - by talking to the app's `metrics` endpoint in case the metric in question - `response_time` is reported by the app.
* [metrics2.py](app/metrics2.py) simply talks to the app and reports `response_time` as a time it takes to complete an HTTP request against the app's URL.

Notes on [app-deployment-sample.yaml](app/app-deployment-sample.yaml):
* The app portion of this file is for reference only
* The important part is [lines 22-34](app/app-deployment-sample.yaml#L22-L34); they should be copied to the real deployment file of the app
* Replace `<AWS_ACCOUNT_ID>` with the real AWS Account Number where the app is running.

## Prometheus

Follow [this guide](https://devopscube.com/setup-prometheus-monitoring-on-kubernetes/) to install Prometheus on your Kubernetes cluster.

**IMPORTANT**: use [this version of config-map.yaml](prometheus/config-map.yaml) for Prometheus deployment instead of the file in the guide above. (The difference is [lines 138-139](prometheus/config-map.yaml#L138-L139))

## Grafana

Grafana dashboard for monitoring app's `response_time`, along with some Kubernetes stats is [here](grafana/web-app-dashboard.json). To import the dashboard, log into Grafana, then click on Dashboards > New > Import.

## See Also
* [Installing AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
* [Installing AWS eksctl](https://docs.aws.amazon.com/eks/latest/userguide/eksctl.html)
* Installing `kubectl`
    * [AWS specific](https://docs.aws.amazon.com/eks/latest/userguide/install-kubectl.html)
    * [Kubernetes generic](https://kubernetes.io/docs/tasks/tools/)
* [Setting up Prometheus on Kubernetes Cluster](https://devopscube.com/setup-prometheus-monitoring-on-kubernetes/)