import os
import time
from prometheus_client import start_http_server, Gauge
import requests

class AppMetrics:
    """AppMetrics Class"""

    def __init__(self, app_port=8000, poll_interval=60):
        self.app_port = app_port
        self.poll_interval = poll_interval

        # Prometheus metrics to collect
        self.response_time = Gauge("response_time", "Response Time")

    def run_metrics_loop(self):
        """Metrics fetching loop"""
        
        while True:
            self.fetch()
            time.sleep(self.poll_interval)

    def fetch(self):
        """Get metrics from the app and refresh Prometheus with them"""

        # Fetch raw status data from the app
        r = requests.get(url=f"http://localhost:{self.app_port}/metrics")
        status_data = r.json()

        # Update Prometheus metrics with app metrics
        self.response_time.set(status_data["response_time"])

def main():
    poll_interval = int(os.getenv("POLL_INTERVAL", "60"))
    app_port = int(os.getenv("APP_PORT", "80"))
    exporter_port = int(os.getenv("EXPORTER_PORT", "9877"))

    app_metrics = AppMetrics(
        app_port=app_port,
        poll_interval=poll_interval
    )
    start_http_server(exporter_port)
    app_metrics.run_metrics_loop()

if __name__ == "__main__":
    main()